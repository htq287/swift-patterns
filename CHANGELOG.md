# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.0.1] - 2019-07-12
### Added
- Create swift-patterns repository
- UnitOfWork pattern: UnitOfWork framework, UnitTests and Sample
- Add README.md
- Add CHANGELOG.md
- Add MIT LICENSE